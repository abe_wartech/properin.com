<?php

namespace App\Http\Controllers;

use App\Article;
use App\Home;
use App\Investor;
use App\Project;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function project()
    {
        $data = Project::take(3)->get();
        return response()->json([
            'success' => true,
            'message' => $data,
        ], 200);
    }

    public function allproject()
    {
        $data = Project::all();
        return response()->json([
            'success' => true,
            'message' => $data,
        ], 200);
    }

    public function article()
    {
        $data = Article::take(4)->orderBy('created_at', 'DESC')->get();
        return response()->json([
            'success' => true,
            'message' => $data,
        ], 200);
    }

    public function home()
    {
        $data = Home::latest('created_at')->first();
        return response()->json([
            'success' => true,
            'message' => $data,
        ], 200);
    }

    public function oneArticle()
    {
        $data = Article::where('id', request('id'))->get();
        return response()->json([
            'success' => true,
            'message' => $data,
        ], 200);
    }

    public function invest()
    {
        $invest = new Investor;
        $invest->project_id = request('project_id');
        $invest->project_name = request('project_name');
        $invest->no_tlp = request('no_tlp');
        $invest->email = request('email');
        $invest->save();
        return response()->json([
            'success' => true,
            'message' => $invest,
        ], 200);
    }
}
