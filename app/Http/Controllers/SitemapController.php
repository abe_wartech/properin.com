<?php

namespace App\Http\Controllers;

use Spatie\Sitemap\SitemapGenerator;

class SitemapController extends Controller
{
    public function index()
    {
        SitemapGenerator::create('https://properin.com')->writeToFile(public_path('sitemap.xml'));
    }
}
