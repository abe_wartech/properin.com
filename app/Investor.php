<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investor extends Model
{
    protected $table = 'investors';

    protected $fillable = ['project_id', 'project_name', 'no_tlp', 'email'];
}
