<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projects', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('name', 65535)->nullable();
			$table->text('pledge', 65535)->nullable();
			$table->text('raised', 65535)->nullable();
			$table->text('return', 65535)->nullable();
			$table->text('duration', 65535)->nullable();
			$table->text('goal', 65535)->nullable();
			$table->text('raised_idr', 65535)->nullable();
			$table->text('time_remaining', 65535)->nullable();
			$table->timestamps();
			$table->text('image', 16777215)->nullable();
			$table->text('harga')->nullable();
			$table->text('tempat')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('projects');
	}

}
