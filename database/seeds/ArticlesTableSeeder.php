<?php

use Illuminate\Database\Seeder;

class ArticlesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('articles')->delete();
        
        \DB::table('articles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => '5 Metode Investasi Properti yang Profitable!',
                'desc' => 'Mengenal lebih jauh alternatif properti yang dapat menjadi sumber investasi beserta masing-masing kelebihannya.',
                'image' => 'articles\\March2020\\NHH5MU0cNBkIzbK6jhOZ.png',
                'created_at' => '2020-03-19 11:42:00',
                'updated_at' => '2020-03-20 10:23:16',
            ),
            1 => 
            array (
                'id' => 10,
                'title' => '5 Metode Investasi Properti yang Profitable!',
                'desc' => 'Mengenal lebih jauh alternatif properti yang dapat menjadi sumber investasi beserta masing-masing kelebihannya.',
                'image' => 'articles\\March2020\\NHH5MU0cNBkIzbK6jhOZ.png',
                'created_at' => '2020-03-19 11:42:00',
                'updated_at' => '2020-03-20 10:23:16',
            ),
            2 => 
            array (
                'id' => 11,
                'title' => '5 Metode Investasi Properti yang Profitable!',
                'desc' => 'Mengenal lebih jauh alternatif properti yang dapat menjadi sumber investasi beserta masing-masing kelebihannya.',
                'image' => 'articles\\March2020\\NHH5MU0cNBkIzbK6jhOZ.png',
                'created_at' => '2020-03-19 11:42:00',
                'updated_at' => '2020-03-20 10:23:16',
            ),
            3 => 
            array (
                'id' => 12,
                'title' => '5 Metode Investasi Properti yang Profitable!',
                'desc' => 'Mengenal lebih jauh alternatif properti yang dapat menjadi sumber investasi beserta masing-masing kelebihannya.',
                'image' => 'articles\\March2020\\NHH5MU0cNBkIzbK6jhOZ.png',
                'created_at' => '2020-03-19 11:42:00',
                'updated_at' => '2020-03-20 10:23:16',
            ),
            4 => 
            array (
                'id' => 13,
                'title' => '5 Metode Investasi Properti yang Profitable!',
                'desc' => 'Mengenal lebih jauh alternatif properti yang dapat menjadi sumber investasi beserta masing-masing kelebihannya.',
                'image' => 'articles\\March2020\\NHH5MU0cNBkIzbK6jhOZ.png',
                'created_at' => '2020-03-19 11:42:00',
                'updated_at' => '2020-03-20 10:23:16',
            ),
        ));
        
        
    }
}