<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'key' => 'browse_admin',
                'table_name' => NULL,
                'created_at' => '2020-03-18 02:24:23',
                'updated_at' => '2020-03-18 02:24:23',
            ),
            1 => 
            array (
                'id' => 2,
                'key' => 'browse_bread',
                'table_name' => NULL,
                'created_at' => '2020-03-18 02:24:23',
                'updated_at' => '2020-03-18 02:24:23',
            ),
            2 => 
            array (
                'id' => 3,
                'key' => 'browse_database',
                'table_name' => NULL,
                'created_at' => '2020-03-18 02:24:23',
                'updated_at' => '2020-03-18 02:24:23',
            ),
            3 => 
            array (
                'id' => 4,
                'key' => 'browse_media',
                'table_name' => NULL,
                'created_at' => '2020-03-18 02:24:23',
                'updated_at' => '2020-03-18 02:24:23',
            ),
            4 => 
            array (
                'id' => 5,
                'key' => 'browse_compass',
                'table_name' => NULL,
                'created_at' => '2020-03-18 02:24:23',
                'updated_at' => '2020-03-18 02:24:23',
            ),
            5 => 
            array (
                'id' => 6,
                'key' => 'browse_menus',
                'table_name' => 'menus',
                'created_at' => '2020-03-18 02:24:23',
                'updated_at' => '2020-03-18 02:24:23',
            ),
            6 => 
            array (
                'id' => 7,
                'key' => 'read_menus',
                'table_name' => 'menus',
                'created_at' => '2020-03-18 02:24:23',
                'updated_at' => '2020-03-18 02:24:23',
            ),
            7 => 
            array (
                'id' => 8,
                'key' => 'edit_menus',
                'table_name' => 'menus',
                'created_at' => '2020-03-18 02:24:23',
                'updated_at' => '2020-03-18 02:24:23',
            ),
            8 => 
            array (
                'id' => 9,
                'key' => 'add_menus',
                'table_name' => 'menus',
                'created_at' => '2020-03-18 02:24:23',
                'updated_at' => '2020-03-18 02:24:23',
            ),
            9 => 
            array (
                'id' => 10,
                'key' => 'delete_menus',
                'table_name' => 'menus',
                'created_at' => '2020-03-18 02:24:23',
                'updated_at' => '2020-03-18 02:24:23',
            ),
            10 => 
            array (
                'id' => 11,
                'key' => 'browse_roles',
                'table_name' => 'roles',
                'created_at' => '2020-03-18 02:24:23',
                'updated_at' => '2020-03-18 02:24:23',
            ),
            11 => 
            array (
                'id' => 12,
                'key' => 'read_roles',
                'table_name' => 'roles',
                'created_at' => '2020-03-18 02:24:23',
                'updated_at' => '2020-03-18 02:24:23',
            ),
            12 => 
            array (
                'id' => 13,
                'key' => 'edit_roles',
                'table_name' => 'roles',
                'created_at' => '2020-03-18 02:24:23',
                'updated_at' => '2020-03-18 02:24:23',
            ),
            13 => 
            array (
                'id' => 14,
                'key' => 'add_roles',
                'table_name' => 'roles',
                'created_at' => '2020-03-18 02:24:23',
                'updated_at' => '2020-03-18 02:24:23',
            ),
            14 => 
            array (
                'id' => 15,
                'key' => 'delete_roles',
                'table_name' => 'roles',
                'created_at' => '2020-03-18 02:24:23',
                'updated_at' => '2020-03-18 02:24:23',
            ),
            15 => 
            array (
                'id' => 16,
                'key' => 'browse_users',
                'table_name' => 'users',
                'created_at' => '2020-03-18 02:24:23',
                'updated_at' => '2020-03-18 02:24:23',
            ),
            16 => 
            array (
                'id' => 17,
                'key' => 'read_users',
                'table_name' => 'users',
                'created_at' => '2020-03-18 02:24:23',
                'updated_at' => '2020-03-18 02:24:23',
            ),
            17 => 
            array (
                'id' => 18,
                'key' => 'edit_users',
                'table_name' => 'users',
                'created_at' => '2020-03-18 02:24:23',
                'updated_at' => '2020-03-18 02:24:23',
            ),
            18 => 
            array (
                'id' => 19,
                'key' => 'add_users',
                'table_name' => 'users',
                'created_at' => '2020-03-18 02:24:23',
                'updated_at' => '2020-03-18 02:24:23',
            ),
            19 => 
            array (
                'id' => 20,
                'key' => 'delete_users',
                'table_name' => 'users',
                'created_at' => '2020-03-18 02:24:23',
                'updated_at' => '2020-03-18 02:24:23',
            ),
            20 => 
            array (
                'id' => 21,
                'key' => 'browse_settings',
                'table_name' => 'settings',
                'created_at' => '2020-03-18 02:24:23',
                'updated_at' => '2020-03-18 02:24:23',
            ),
            21 => 
            array (
                'id' => 22,
                'key' => 'read_settings',
                'table_name' => 'settings',
                'created_at' => '2020-03-18 02:24:23',
                'updated_at' => '2020-03-18 02:24:23',
            ),
            22 => 
            array (
                'id' => 23,
                'key' => 'edit_settings',
                'table_name' => 'settings',
                'created_at' => '2020-03-18 02:24:23',
                'updated_at' => '2020-03-18 02:24:23',
            ),
            23 => 
            array (
                'id' => 24,
                'key' => 'add_settings',
                'table_name' => 'settings',
                'created_at' => '2020-03-18 02:24:23',
                'updated_at' => '2020-03-18 02:24:23',
            ),
            24 => 
            array (
                'id' => 25,
                'key' => 'delete_settings',
                'table_name' => 'settings',
                'created_at' => '2020-03-18 02:24:23',
                'updated_at' => '2020-03-18 02:24:23',
            ),
            25 => 
            array (
                'id' => 26,
                'key' => 'browse_projects',
                'table_name' => 'projects',
                'created_at' => '2020-03-18 02:27:54',
                'updated_at' => '2020-03-18 02:27:54',
            ),
            26 => 
            array (
                'id' => 27,
                'key' => 'read_projects',
                'table_name' => 'projects',
                'created_at' => '2020-03-18 02:27:54',
                'updated_at' => '2020-03-18 02:27:54',
            ),
            27 => 
            array (
                'id' => 28,
                'key' => 'edit_projects',
                'table_name' => 'projects',
                'created_at' => '2020-03-18 02:27:54',
                'updated_at' => '2020-03-18 02:27:54',
            ),
            28 => 
            array (
                'id' => 29,
                'key' => 'add_projects',
                'table_name' => 'projects',
                'created_at' => '2020-03-18 02:27:54',
                'updated_at' => '2020-03-18 02:27:54',
            ),
            29 => 
            array (
                'id' => 30,
                'key' => 'delete_projects',
                'table_name' => 'projects',
                'created_at' => '2020-03-18 02:27:54',
                'updated_at' => '2020-03-18 02:27:54',
            ),
            30 => 
            array (
                'id' => 31,
                'key' => 'browse_articles',
                'table_name' => 'articles',
                'created_at' => '2020-03-19 11:38:40',
                'updated_at' => '2020-03-19 11:38:40',
            ),
            31 => 
            array (
                'id' => 32,
                'key' => 'read_articles',
                'table_name' => 'articles',
                'created_at' => '2020-03-19 11:38:40',
                'updated_at' => '2020-03-19 11:38:40',
            ),
            32 => 
            array (
                'id' => 33,
                'key' => 'edit_articles',
                'table_name' => 'articles',
                'created_at' => '2020-03-19 11:38:40',
                'updated_at' => '2020-03-19 11:38:40',
            ),
            33 => 
            array (
                'id' => 34,
                'key' => 'add_articles',
                'table_name' => 'articles',
                'created_at' => '2020-03-19 11:38:40',
                'updated_at' => '2020-03-19 11:38:40',
            ),
            34 => 
            array (
                'id' => 35,
                'key' => 'delete_articles',
                'table_name' => 'articles',
                'created_at' => '2020-03-19 11:38:40',
                'updated_at' => '2020-03-19 11:38:40',
            ),
        ));
        
        
    }
}