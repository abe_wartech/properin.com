<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'role_id' => 1,
                'name' => 'Admin',
                'email' => 'admin@database.com',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$vPrnAOikDyPyl0R/Nw4dL.peo5UbVETSwyp4zPiqX5n7wx2dbP8x2',
                'remember_token' => NULL,
                'settings' => NULL,
                'created_at' => '2020-02-27 12:29:19',
                'updated_at' => '2020-02-27 12:29:20',
            ),
            1 => 
            array (
                'id' => 2,
                'role_id' => 2,
                'name' => 'User',
                'email' => 'admin@properin.com',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$TzaxiXnYo3GfaFT2y5mfS.ycL5UQgC8e5q3u/s760tRD1XiRf.YPO',
                'remember_token' => NULL,
                'settings' => '{"locale":"en"}',
                'created_at' => '2020-02-27 12:45:18',
                'updated_at' => '2020-02-27 12:45:18',
            ),
        ));
        
        
    }
}