import React, { Component } from "react";
import { Row, Col, Typography, Button } from "antd";
import AOS from "aos";
import { Link } from "react-router-dom";
const { Title } = Typography;
import { inject, observer } from "mobx-react";

@inject("newsStore")
@observer
class Section1 extends Component {
    state = {
        width: window.innerWidth,
        visible: false,
        confirmLoading: false
    };
    componentDidMount = () => {
        AOS.init({
            duration: 1000,
            delay: 50
        });
    };
    handleOk = () => {
        this.setState({
            confirmLoading: true
        });
        setTimeout(() => {
            this.setState({
                visible: false,
                confirmLoading: false
            });
        }, 2000);
    };
    handleCancel = () => {
        this.setState({
            visible: false
        });
    };
    render() {
        const { dataHome } = this.props.newsStore;
        return (
            <div>
                <Row
                    className="section1"
                    type="flex"
                    justify="center"
                    align="middle"
                >
                    <Col xs={24} md={18}>
                        <Title
                            data-aos="zoom-in-up"
                            level={1}
                            style={{ fontSize: "3.2vw", color: "white" }}
                        >
                            Mendorong Pembangunan dan Kemajuan dengan Investasi
                            di Properti
                        </Title>
                        <Row
                            data-aos="slide-right"
                            data-aos-delay="850"
                            style={{ marginTop: "4rem" }}
                        >
                            <Link to="/project">
                                <Button
                                    className="hoverbutton1"
                                    type="primary"
                                    size="large"
                                >
                                    Invest Sekarang
                                </Button>
                            </Link>
                        </Row>
                    </Col>
                </Row>
                <Row align="middle" className="feature-properin">
                    <Col md={6} align="center">
                        <Typography className="text-card-section1">
                            {dataHome.properti}
                        </Typography>
                        <Typography style={{ fontSize: "1.2vw" }}>
                            Properti
                        </Typography>
                    </Col>
                    <Col md={6} align="center">
                        <Typography className="text-card-section1">
                            {dataHome.investor}
                        </Typography>
                        <Typography style={{ fontSize: "1.2vw" }}>
                            Investor
                        </Typography>
                    </Col>
                    <Col md={6} align="center">
                        <Typography className="text-card-section1">
                            {dataHome.dana}
                        </Typography>
                        <Typography style={{ fontSize: "1.2vw" }}>
                            Dana Terkumpul
                        </Typography>
                    </Col>
                    <Col md={6} align="center">
                        <Typography className="text-card-section1">
                            {dataHome.hasil}
                        </Typography>
                        <Typography style={{ fontSize: "1.2vw" }}>
                            Hasil Bagi Investasi
                        </Typography>
                    </Col>
                </Row>
                <Row className="row-sect1" justify="center">
                    <Col md={8} align="left" justify="center">
                        <Typography className="prop-about">
                            Tentang Properin
                        </Typography>
                    </Col>
                    <Col md={16} align="left">
                        <Typography
                            style={{ fontSize: "1.2vw", color: "white" }}
                        >
                            Properin.com merupakan sebuah online platform untuk
                            berinvestasi di bidang properti yang menghubungkan
                            antara pihak investor dengan pihak yang memerlukan
                            pembiayaan proyek maupun renovasi properti untuk
                            dijual kembali (flipping) melalui proses bagi hasil
                            yang mudah dana aman.
                        </Typography>
                    </Col>
                </Row>
            </div>
        );
    }
}
export default Section1;
