import React, { Component } from "react";
import { Row, Col, Typography, Button } from "antd";
import { inject, observer } from "mobx-react";
import AOS from "aos";
import { Link } from "react-router-dom";
const { Title } = Typography;

@inject("newsStore")
@observer
class Section1Mobile extends Component {
    state = {
        width: window.innerWidth,
        visible: false,
        confirmLoading: false
    };
    componentDidMount = () => {
        AOS.init({
            duration: 1000,
            delay: 50
        });
    };

    UNSAFE_componentWillMount() {
        window.addEventListener("resize", this.handleWindowSizeChange);
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.handleWindowSizeChange);
    }

    handleWindowSizeChange = () => {
        this.setState({ width: window.innerWidth });
    };
    render() {
        const { dataHome } = this.props.newsStore;
        return (
            <div>
                {" "}
                <Row
                    className="section1-mobile"
                    type="flex"
                    justify="center"
                    align="middle"
                >
                    <Col xs={24} md={12} align="left">
                        <Title
                            data-aos="zoom-in-up"
                            level={1}
                            style={{ fontSize: "5.5vw", color: "white" }}
                        >
                            Mendorong Pembangunan dan Kemajuan dengan Investasi
                            di Properti
                        </Title>
                        <Row
                            data-aos="slide-right"
                            data-aos-delay="850"
                            style={{ marginTop: "3rem" }}
                        >
                            <Link to="/project">
                                <Button
                                    className="hoverbutton1"
                                    type="primary"
                                    size="large"
                                >
                                    Invest Sekarang
                                </Button>
                            </Link>
                        </Row>
                    </Col>
                </Row>
                <Row align="middle" className="feature-properin-mobile">
                    <Col
                        xs={12}
                        md={12}
                        align="center"
                        style={{ paddingBottom: "0.5rem" }}
                    >
                        <Typography
                            style={{
                                fontSize: "5vw",
                                fontWeight: "bold",
                                color: "#23ad46"
                            }}
                        >
                            {dataHome.properti}
                        </Typography>
                        <Typography style={{ fontSize: "3vw" }}>
                            Properti
                        </Typography>
                    </Col>
                    <Col
                        xs={12}
                        md={12}
                        align="center"
                        style={{ paddingBottom: "0.5rem" }}
                    >
                        <Typography
                            style={{
                                fontSize: "5vw",
                                fontWeight: "bold",
                                color: "#23ad46"
                            }}
                        >
                            {dataHome.investor}
                        </Typography>
                        <Typography style={{ fontSize: "3vw" }}>
                            Investor
                        </Typography>
                    </Col>
                    <Col xs={12} md={12} align="center">
                        <Typography
                            style={{
                                fontSize: "5vw",
                                fontWeight: "bold",
                                color: "#23ad46"
                            }}
                        >
                            {dataHome.dana}
                        </Typography>
                        <Typography style={{ fontSize: "3vw" }}>
                            Dana Terkumpul
                        </Typography>
                    </Col>
                    <Col xs={12} md={12} align="center">
                        <Typography
                            style={{
                                fontSize: "5vw",
                                fontWeight: "bold",
                                color: "#23ad46"
                            }}
                        >
                            {dataHome.hasil}
                        </Typography>
                        <Typography style={{ fontSize: "3vw" }}>
                            Hasil Bagi Investasi
                        </Typography>
                    </Col>
                </Row>
                <Row
                    className="row-sect1-mobile"
                    justify="center"
                    align="middle"
                >
                    <Col xs={24} md={24} align="center">
                        <Typography className="prop-about-mobile">
                            Tentang Properin
                        </Typography>
                    </Col>
                    <Col xs={24} md={24} align="center">
                        <Typography style={{ fontSize: "3vw", color: "white" }}>
                            Properin.com merupakan sebuah online platform untuk
                            berinvestasi di bidang properti yang menghubungkan
                            antara pihak investor dengan pihak yang memerlukan
                            pembiayaan proyek maupun renovasi properti untuk
                            dijual kembali (flipping) melalui proses bagi hasil
                            yang mudah dana aman.
                        </Typography>
                    </Col>
                </Row>
            </div>
        );
    }
}
export default Section1Mobile;
