import React, { Component } from "react";
import { Row, Col, Typography } from "antd";
import AOS from "aos";

const { Title } = Typography;

class Section2 extends Component {
    state = {
        width: window.innerWidth,
        visible: false,
        confirmLoading: false
    };
    componentDidMount = () => {
        AOS.init({
            duration: 1000,
            delay: 50
        });
    };

    showModal = () => {
        this.setState({
            visible: true
        });
    };
    handleOk = () => {
        this.setState({
            confirmLoading: true
        });
        setTimeout(() => {
            this.setState({
                visible: false,
                confirmLoading: false
            });
        }, 2000);
    };
    handleCancel = () => {
        this.setState({
            visible: false
        });
    };
    render() {
        return (
            <div>
                <Row className="section2" justify="center">
                    <Col
                        data-aos="slide-right"
                        data-aos-delay="550"
                        md={24}
                        align="center"
                        style={{ marginBottom: "2rem" }}
                    >
                        <Title
                            level={4}
                            className="text-white"
                            style={{ fontSize: "1.9vw" }}
                        >
                            Mengapa Berinvestasi di Properin ?
                        </Title>
                        <img
                            src={`${window.location.origin}/images/ornament.png`}
                        />
                    </Col>
                    <Row
                        type="flex"
                        gutter={48}
                        justify="space-around"
                        data-aos="slide-right"
                    >
                        <Col md={6} align="center">
                            <img
                                src={`${window.location.pathname}images/invest1.png`}
                                className="img1"
                            />
                            <Title level={4} className="text-white">
                                Menguntungkan
                            </Title>
                            <Typography style={{ fontSize: "1.2vw" }}>
                                Properti merupakan asset investasi dengan
                                pertumbuhan nilai jual yang baik.
                            </Typography>
                        </Col>
                        <Col md={6} align="center">
                            <img
                                src={`${window.location.pathname}images/invest2.svg`}
                                className="img2"
                            />
                            <Title level={4} className="text-white">
                                Transparansi
                            </Title>
                            <Typography style={{ fontSize: "1.2vw" }}>
                                Skema keuntungan dari proses bagi hasil yang
                                jelas dan transparan.
                            </Typography>
                        </Col>
                        <Col md={6} align="center">
                            <img
                                src={`${window.location.pathname}images/invest3.svg`}
                                className="img3"
                            />
                            <Title level={4} className="text-white">
                                Mudah
                            </Title>
                            <Typography style={{ fontSize: "1.2vw" }}>
                                Proses yang cepat, mudah, dan sederhana.
                            </Typography>
                        </Col>
                        <Col md={6} align="center">
                            <img
                                src={`${window.location.pathname}images/invest4.svg`}
                                className="img4"
                            />
                            <Title level={4} className="text-white">
                                Berbasis Komunitas
                            </Title>
                            <Typography style={{ fontSize: "1.2vw" }}>
                                Kami memiliki komunitas di bidang property yang
                                dapat menambah jaringan untuk hubungan kerjasama
                                jangka panjang.
                            </Typography>
                        </Col>
                    </Row>
                </Row>
            </div>
        );
    }
}
export default Section2;
