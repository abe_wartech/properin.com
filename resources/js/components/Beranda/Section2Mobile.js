import React, { Component } from "react";
import { Row, Col, Typography } from "antd";
import { inject, observer } from "mobx-react";
import AOS from "aos";
import { Link } from "react-router-dom";
const { Title } = Typography;

@inject("newsStore")
@observer
class Section2Mobile extends Component {
    state = {
        width: window.innerWidth,
        visible: false,
        confirmLoading: false
    };
    componentDidMount = () => {
        AOS.init({
            duration: 1000,
            delay: 50
        });
    };

    UNSAFE_componentWillMount() {
        window.addEventListener("resize", this.handleWindowSizeChange);
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.handleWindowSizeChange);
    }

    handleWindowSizeChange = () => {
        this.setState({ width: window.innerWidth });
    };
    render() {
        const { width, visible, confirmLoading } = this.state;

        return (
            <div>
                <Row className="section2-mobile" justify="center">
                    <Col
                        data-aos="slide-right"
                        data-aos-delay="550"
                        xs={24}
                        align="center"
                    >
                        <Typography
                            className="text-white"
                            style={{ fontSize: "5vw" }}
                        >
                            Mengapa Berinvestasi di Properin ?
                        </Typography>
                        <img
                            src={`${window.location.origin}/images/ornament.png`}
                        />
                    </Col>
                    <Row
                        type="flex"
                        justify="space-around"
                        style={{ padding: 25 }}
                        data-aos="slide-right"
                    >
                        <Col
                            xs={24}
                            md={12}
                            align="center"
                            style={{ paddingBottom: "1rem" }}
                        >
                            <img
                                src={`${window.location.pathname}images/invest1-mobile.png`}
                                className="img1"
                            />
                            <Typography className="text-white-mobile">
                                Menguntungkan
                            </Typography>
                            <Typography style={{ fontSize: "3vw" }}>
                                Properti merupakan asset investasi dengan
                                pertumbuhan nilai jual yang baik.
                            </Typography>
                        </Col>
                        <Col
                            xs={24}
                            md={12}
                            align="center"
                            style={{ paddingBottom: "1rem" }}
                        >
                            <img
                                src={`${window.location.pathname}images/invest2-mobile.svg`}
                                className="img2"
                            />
                            <Typography className="text-white-mobile">
                                Transparansi
                            </Typography>
                            <Typography style={{ fontSize: "3vw" }}>
                                Skema keuntungan dari proses bagi hasil yang
                                jelas dan transparan.
                            </Typography>
                        </Col>
                        <Col
                            xs={24}
                            md={12}
                            align="center"
                            style={{ paddingBottom: "1rem" }}
                        >
                            <img
                                src={`${window.location.pathname}images/invest3-mobile.svg`}
                                className="img3"
                            />
                            <Typography className="text-white-mobile">
                                Mudah
                            </Typography>
                            <Typography style={{ fontSize: "3vw" }}>
                                Proses yang cepat, mudah, dan sederhana.
                            </Typography>
                        </Col>
                        <Col
                            xs={24}
                            md={12}
                            align="center"
                            style={{ paddingBottom: "1rem" }}
                        >
                            <img
                                src={`${window.location.pathname}images/invest4-mobile.svg`}
                                className="img4"
                            />
                            <Typography className="text-white-mobile">
                                Berbasis Komunitas
                            </Typography>
                            <Typography style={{ fontSize: "3vw" }}>
                                Kami memiliki komunitas di bidang property yang
                                dapat menambah jaringan untuk hubungan kerjasama
                                jangka panjang.
                            </Typography>
                        </Col>
                    </Row>
                </Row>
            </div>
        );
    }
}
export default Section2Mobile;
