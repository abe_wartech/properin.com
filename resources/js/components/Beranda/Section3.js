import React, { Component } from "react";
import {
    Row,
    Col,
    Typography,
    Card,
    Progress,
    Divider,
    Button,
    Modal,
    Form,
    Input,
    message,
} from "antd";
import { Link } from "react-router-dom";
import AOS from "aos";
import { inject, observer } from "mobx-react";

const { Title } = Typography;

@inject("newsStore")
@observer
class Section3 extends Component {
    state = {
        width: window.innerWidth,
        visible: false,
        confirmLoading: false,
        visible2: false,
        eproject_id: "",
        project_name: "",
        no_tlp: "",
        email: "",
        confirmLoading2: false,
    };
    componentDidMount = () => {
        AOS.init({
            duration: 1000,
            delay: 50,
        });
    };

    showModal = (id, name) => {
        this.setState({
            visible: true,
            project_id: id,
            project_name: name,
        });
    };
    showModal2 = () => {
        if (this.state.no_tlp === "") {
            message.error("Please enter your no tlp!");
        }
        if (this.state.email === "") {
            message.error("Please enter your email!");
        } else {
            this.setState({
                visible2: true,
                visible: false,
            });
            const data = {
                project_id: this.state.project_id,
                project_name: this.state.project_name,
                no_tlp: this.state.no_tlp,
                email: this.state.email,
            };
            this.props.newsStore.invest(data);
        }
    };
    handleOk = () => {
        this.setState({
            confirmLoading: true,
        });
        setTimeout(() => {
            this.setState({
                visible: false,
                confirmLoading: false,
            });
        }, 2000);
    };
    handleOk2 = () => {
        this.setState({
            confirmLoading2: true,
        });
        setTimeout(() => {
            this.setState({
                visible2: false,
                confirmLoading2: false,
            });
        }, 2000);
    };
    handleCancel = () => {
        this.setState({
            visible: false,
        });
    };
    handleCancel2 = () => {
        this.setState({
            visible2: false,
        });
    };
    onChangeTelp = (event) => {
        this.setState({ no_tlp: event.target.value });
    };
    onChangeEmail = (event) => {
        this.setState({ email: event.target.value });
    };
    render() {
        const {
            visible,
            confirmLoading,
            visible2,
            confirmLoading2,
        } = this.state;
        const { dataNews } = this.props.newsStore;
        return (
            <div>
                <Row className="section3">
                    <Col
                        data-aos="slide-right"
                        md={24}
                        align="center"
                        style={{ marginBottom: "2rem" }}
                    >
                        <Title
                            level={4}
                            className="text-white"
                            style={{ fontSize: "1.9vw" }}
                        >
                            Investasi Proyek Terbaru
                        </Title>
                        <img
                            src={`${window.location.origin}/images/ornament.png`}
                        />
                    </Col>
                    <Row
                        type="flex"
                        gutter={55}
                        justify="space-around"
                        data-aos="slide-right"
                    >
                        {dataNews.map((item) => (
                            <Col md={8} align="center" key={item.id}>
                                <Card
                                    className="card-invest"
                                    cover={
                                        <img
                                            src={`${window.location.origin}/storage/${item.image}`}
                                        />
                                    }
                                >
                                    <Typography className="title-invest">
                                        {item.name}
                                    </Typography>
                                    <div className="padding-invest">
                                        <img
                                            src={`${window.location.origin}/images/label.svg`}
                                            className="textinfo"
                                        />
                                        <Typography className="textinfodesc">
                                            {item.harga}
                                        </Typography>
                                        <img
                                            src={`${window.location.origin}/images/loc.svg`}
                                            className="textinfo"
                                        />
                                        <Typography className="textinfodesc">
                                            {item.name}
                                        </Typography>
                                    </div>
                                    <div className="padding-invest">
                                        <Typography className="textinfoprogress">
                                            Pledge
                                        </Typography>
                                        <Progress
                                            percent={item.pledge}
                                            strokeColor="#23ad46"
                                        />
                                        <Typography>Raised</Typography>
                                        <Progress
                                            percent={item.raised}
                                            strokeColor="#23ad46"
                                            style={{ marginBottom: "2rem" }}
                                        />
                                        <br />
                                        <Typography className="insideleft">
                                            Projected Return
                                        </Typography>
                                        <Typography className="inside">
                                            {item.return}
                                        </Typography>
                                        <Divider />
                                        <Typography className="insideleft">
                                            Duration
                                        </Typography>
                                        <Typography className="inside">
                                            {item.duration}
                                        </Typography>
                                        <Divider />
                                        <Typography className="insideleft">
                                            Goal
                                        </Typography>
                                        <Typography className="inside">
                                            {item.goal}
                                        </Typography>
                                        <Divider />
                                        <Typography className="insideleft">
                                            Raised
                                        </Typography>
                                        <Typography className="inside">
                                            {item.raised_idr}
                                        </Typography>
                                        <Divider />
                                        <Typography className="insideleft">
                                            Time Remaining
                                        </Typography>
                                        <Typography className="inside">
                                            {item.time_remaining}
                                        </Typography>
                                        <Divider />
                                    </div>
                                    <Button
                                        className="hoverbutton"
                                        block
                                        onClick={() => {
                                            this.showModal(item.id, item.name);
                                        }}
                                    >
                                        Invest
                                    </Button>
                                </Card>
                            </Col>
                        ))}
                    </Row>
                </Row>
                <Row data-aos="fade-down-right">
                    <Col md={24} align="center" className="col-sec3">
                        <Link to="/project">
                            <Button block className="button-more">
                                Lihat Lebih Banyak
                            </Button>
                        </Link>
                    </Col>
                </Row>
                <Modal
                    width={"75vw"}
                    visible={visible}
                    onOk={this.handleOk}
                    confirmLoading={confirmLoading}
                    onCancel={this.handleCancel}
                    footer={null}
                >
                    <Row className="modal-popup" align="middle">
                        <Col md={12}>
                            <img
                                src={`${window.location.origin}/images/popupinvest.svg`}
                            />
                            <Title
                                style={{
                                    fontSize: "1.4vw",
                                    paddingTop: "1rem",
                                }}
                            >
                                Masukkan Informasi Kontak Untuk
                                <br />
                                Memulai Investasi Anda di{" "}
                                <span style={{ color: "#23ad46" }}>
                                    Properin
                                </span>
                            </Title>
                        </Col>
                        <Col md={12} style={{ paddingLeft: "4rem" }}>
                            <Form>
                                <Form.Item>
                                    <Title style={{ fontSize: "1vw" }}>
                                        Nomer Telepon
                                    </Title>
                                    <Input
                                        onChange={this.onChangeTelp}
                                        placeholder="0812 3846 xxxx"
                                    />
                                </Form.Item>
                                <Form.Item>
                                    <Title style={{ fontSize: "1vw" }}>
                                        E-Mail
                                    </Title>
                                    <Input
                                        onChange={this.onChangeEmail}
                                        placeholder="contoh@gmail.com"
                                    />
                                </Form.Item>
                            </Form>
                            <Row justify="end">
                                <Col>
                                    <Button
                                        onClick={this.handleCancel}
                                        style={{ color: "#23ad46" }}
                                        type="link"
                                    >
                                        Batal
                                    </Button>
                                    <Button
                                        onClick={this.showModal2}
                                        type="primary"
                                        style={{
                                            backgroundColor: "#23ad46",
                                            border: "none",
                                        }}
                                    >
                                        Kirim
                                    </Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Modal>
                <Modal
                    width={"75vw"}
                    visible={visible2}
                    onOk={this.handleOk2}
                    confirmLoading={confirmLoading2}
                    onCancel={this.handleCancel2}
                    footer={null}
                >
                    <Row
                        className="modal-popup-thank"
                        align="middle"
                        justify="center"
                    >
                        <Col md={24}>
                            <img
                                src={`${window.location.origin}/images/checklist.svg`}
                                style={{ width: "20vw", height: "25vh" }}
                            />
                            <Typography
                                style={{
                                    fontSize: "2vw",
                                    paddingTop: "1rem",
                                    fontWeight: "bold",
                                }}
                            >
                                Terimakasih
                            </Typography>
                            <Typography
                                style={{
                                    fontSize: "1.6vw",
                                    paddingBottom: "1rem",
                                }}
                            >
                                Team kami akan segera menghubungi anda
                            </Typography>
                            <Link to="/project">
                                <Button className="button-more-thank">
                                    Lihat Lebih Banyak
                                </Button>
                            </Link>
                        </Col>
                    </Row>
                </Modal>
            </div>
        );
    }
}
export default Section3;
