import React, { Component } from "react";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {
    Row,
    Col,
    Typography,
    Button,
    Card,
    Progress,
    Modal,
    Form,
    Input,
    message
} from "antd";
import { inject, observer } from "mobx-react";
import AOS from "aos";
import { Link } from "react-router-dom";

@inject("newsStore")
@observer
class Section3Mobile extends Component {
    state = {
        visible: false,
        confirmLoading: false,
        visible2: false,
        project_id: "",
        project_name: "",
        no_tlp: "",
        email: "",
        confirmLoading2: false
    };
    componentDidMount = () => {
        AOS.init({
            duration: 1000,
            delay: 50
        });
    };
    showModal = (id, name) => {
        this.setState({
            visible: true,
            project_id: id,
            project_name: name
        });
    };
    showModal2 = () => {
        if (this.state.no_tlp === "") {
            message.error("Please enter your no tlp!");
        }
        if (this.state.email === "") {
            message.error("Please enter your email!");
        } else {
            this.setState({
                visible2: true,
                visible: false
            });
            const data = {
                project_id: this.state.project_id,
                project_name: this.state.project_name,
                no_tlp: this.state.no_tlp,
                email: this.state.email
            };
            this.props.newsStore.invest(data);
        }
    };
    handleOk = () => {
        this.setState({
            confirmLoading: true
        });
        setTimeout(() => {
            this.setState({
                visible: false,
                confirmLoading: false
            });
        }, 2000);
    };
    handleOk2 = () => {
        this.setState({
            confirmLoading2: true
        });
        setTimeout(() => {
            this.setState({
                visible2: false,
                confirmLoading2: false
            });
        }, 2000);
    };
    handleCancel = () => {
        this.setState({
            visible: false
        });
    };
    handleCancel2 = () => {
        this.setState({
            visible2: false
        });
    };

    onChangeTelp = event => {
        this.setState({ no_tlp: event.target.value });
    };
    onChangeEmail = event => {
        this.setState({ email: event.target.value });
    };

    render() {
        const {
            visible,
            confirmLoading,
            visible2,
            confirmLoading2
        } = this.state;
        const { dataNews } = this.props.newsStore;
        return (
            <div>
                <Row className="section3-mobile" justify="center">
                    <Col
                        xs={24}
                        md={24}
                        align="center"
                        style={{ paddingBottom: "1rem" }}
                    >
                        <Typography
                            style={{
                                fontSize: "5vw",
                                color: "#23ad46",
                                fontWeight: "bold"
                            }}
                        >
                            Investasi Proyek Terbaru
                        </Typography>
                        <img
                            src={`${window.location.origin}/images/ornament.png`}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col xs={24} md={24} align="center">
                        <div>
                            <Carousel
                                showThumbs={false}
                                autoPlay={true}
                                showIndicators={false}
                                infiniteLoop={true}
                                showStatus={false}
                            >
                                {dataNews.map(item => (
                                    <div key={item.id}>
                                        <Card className="card-invest-mobile">
                                            <img
                                                src={`${window.location.origin}/storage/${item.image}`}
                                            />
                                            <Typography className="title-invest-mobile">
                                                {item.name}
                                            </Typography>
                                            <div className="padding-invest-mobile">
                                                <img
                                                    src={`${window.location.origin}/images/label.svg`}
                                                    className="textinfo-mobile"
                                                />
                                                <Typography className="textinfodesc-mobile">
                                                    {item.harga}
                                                </Typography>
                                                <img
                                                    src={`${window.location.origin}/images/loc.svg`}
                                                    className="textinfo-mobile"
                                                />
                                                <Typography className="textinfodesc-mobile">
                                                    {item.name}
                                                </Typography>
                                            </div>
                                            <div className="padding-invest-mobile">
                                                <Typography>Raised</Typography>
                                                <Progress
                                                    strokeColor="#23ad46"
                                                    percent={item.raised}
                                                    style={{
                                                        marginBottom: "1rem"
                                                    }}
                                                />
                                                <br />
                                            </div>

                                            <Button
                                                className="button-mobile"
                                                type="link"
                                                block
                                            >
                                                Detail
                                            </Button>
                                            <Button
                                                className="hoverbutton-mobile"
                                                block
                                                onClick={() => {
                                                    this.showModal(
                                                        item.id,
                                                        item.name
                                                    );
                                                }}
                                            >
                                                Invest
                                            </Button>
                                        </Card>
                                    </div>
                                ))}
                            </Carousel>
                        </div>
                    </Col>
                </Row>
                <Row data-aos="fade-down-right">
                    <Col md={24} align="center" className="col-sec3-mobile">
                        <Link to="/project">
                            <Button block className="button-more-mobile">
                                Lihat Lebih Banyak
                            </Button>
                        </Link>
                    </Col>
                </Row>
                <Modal
                    width={"80vw"}
                    visible={visible}
                    onOk={this.handleOk}
                    confirmLoading={confirmLoading}
                    onCancel={this.handleCancel}
                    footer={null}
                >
                    <Row align="middle">
                        <Col xs={24} md={24} align="center">
                            <img
                                src={`${window.location.origin}/images/popupinvest.svg`}
                                style={{ width: "57vw", height: "50vw" }}
                            />
                        </Col>
                        <Col xs={24}>
                            <Typography
                                style={{
                                    fontSize: "4vw",
                                    textAlign: "center",
                                    paddingBottom: "1rem"
                                }}
                            >
                                Masukkan Informasi Kontak Untuk Memulai
                                Investasi Anda di{" "}
                                <span style={{ color: "#23ad46" }}>
                                    Properin
                                </span>
                            </Typography>
                        </Col>
                        <Col xs={24}>
                            <Form>
                                <Form.Item>
                                    <Typography style={{ fontSize: "3vw" }}>
                                        Nomer Telepon
                                    </Typography>
                                    <Input placeholder="0812 3846 xxxx" />
                                </Form.Item>
                                <Form.Item>
                                    <Typography style={{ fontSize: "3vw" }}>
                                        E-Mail
                                    </Typography>
                                    <Input placeholder="contoh@gmail.com" />
                                </Form.Item>
                            </Form>
                            <Row justify="end">
                                <Col xs={12}>
                                    <Button
                                        type="link"
                                        style={{ color: "#23ad46" }}
                                        onPress={this.handleCancel}
                                    >
                                        Batal
                                    </Button>
                                </Col>
                                <Col xs={12} align="right">
                                    <Button
                                        onClick={this.showModal2}
                                        type="primary"
                                        style={{
                                            backgroundColor: "#23ad46",
                                            width: "20vw",
                                            border: "none"
                                        }}
                                    >
                                        Kirim
                                    </Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Modal>
                <Modal
                    width={"80vw"}
                    visible={visible2}
                    onOk={this.handleOk2}
                    confirmLoading={confirmLoading2}
                    onCancel={this.handleCancel2}
                    footer={null}
                >
                    <Row align="middle">
                        <Col xs={24} md={24} align="center">
                            <img
                                src={`${window.location.origin}/images/checklist.svg`}
                                style={{ width: "40vw", height: "30vh" }}
                            />
                        </Col>
                        <Col xs={24} align="center">
                            <Typography
                                style={{
                                    fontSize: "5vw",
                                    textAlign: "center",
                                    paddingTop: "1rem",
                                    fontWeight: "bold"
                                }}
                            >
                                Terimakasih
                            </Typography>
                            <Typography
                                style={{
                                    fontSize: "3.5vw",
                                    paddingBottom: "1rem"
                                }}
                            >
                                Team kami akan segera menghubungi anda
                            </Typography>
                            <Link to="/project">
                                <Button
                                    block
                                    className="button-more-mobile-thank"
                                >
                                    Lihat Lebih Banyak
                                </Button>
                            </Link>
                        </Col>
                    </Row>
                </Modal>
            </div>
        );
    }
}
export default Section3Mobile;
