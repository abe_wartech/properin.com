import React, { Component } from "react";
import { Row, Col, Typography, Card } from "antd";
import { inject, observer } from "mobx-react";
import { Link } from "react-router-dom";
import AOS from "aos";

const { Title } = Typography;

@inject("newsStore")
@observer
class Section4 extends Component {
    state = {
        visible: false,
        confirmLoading: false
    };
    componentDidMount = () => {
        AOS.init({
            duration: 1000,
            delay: 50
        });
    };

    showModal = () => {
        this.setState({
            visible: true
        });
    };
    handleOk = () => {
        this.setState({
            confirmLoading: true
        });
        setTimeout(() => {
            this.setState({
                visible: false,
                confirmLoading: false
            });
        }, 2000);
    };
    handleCancel = () => {
        this.setState({
            visible: false
        });
    };
    render() {
        const { dataArticle } = this.props.newsStore;
        return (
            <div>
                <Row className="section4" justify="center">
                    <Col
                        data-aos="slide-right"
                        data-aos-delay="550"
                        xs={24}
                        align="center"
                        style={{ marginBottom: "2rem" }}
                    >
                        <Title
                            level={4}
                            className="text-white"
                            style={{ fontSize: "1.9vw" }}
                        >
                            Berita & Artikel
                        </Title>
                        <img
                            src={`${window.location.origin}/images/ornament.png`}
                        />
                    </Col>
                    <Row
                        type="flex"
                        gutter={48}
                        justify="space-around"
                        data-aos="slide-right"
                    >
                        {dataArticle.map(item => (
                            <Col md={6} align="center" key={item.id}>
                                <Link to={`/news/${item.id}`}>
                                    <Card
                                        className="news-place"
                                        cover={
                                            <img
                                                src={`${window.location.origin}/storage/${item.image}`}
                                            />
                                        }
                                    >
                                        <div className="news-article">
                                            <Typography className="header-news">
                                                {item.title}
                                            </Typography>
                                            <Typography className="news-article-read">
                                                {item.desc}
                                            </Typography>
                                        </div>
                                    </Card>
                                </Link>
                            </Col>
                        ))}
                    </Row>
                </Row>
            </div>
        );
    }
}
export default Section4;
