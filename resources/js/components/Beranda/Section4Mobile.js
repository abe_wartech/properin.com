import React, { Component } from "react";
import { Row, Col, Typography, Card } from "antd";
import { inject, observer } from "mobx-react";
import AOS from "aos";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Link } from "react-router-dom";

@inject("newsStore")
@observer
class Section4Mobile extends Component {
    state = {
        visible: false,
        confirmLoading: false
    };
    componentDidMount = () => {
        AOS.init({
            duration: 1000,
            delay: 50
        });
    };

    handleCancel = () => {
        this.setState({
            visible: false
        });
    };

    render() {
        const { dataArticle } = this.props.newsStore;
        return (
            <div>
                <Row className="section4-mobile" justify="center">
                    <Col
                        md={24}
                        align="center"
                        xs={24}
                        style={{ marginBottom: "1rem" }}
                    >
                        <Typography
                            className="text-white"
                            style={{ fontSize: "5vw" }}
                        >
                            Berita & Artikel
                        </Typography>
                        <img
                            src={`${window.location.origin}/images/ornament.png`}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col xs={24} sm={24} style={{ marginBottom: "3rem" }}>
                        <div>
                            <Carousel
                                showThumbs={false}
                                autoPlay={true}
                                showIndicators={false}
                                infiniteLoop={true}
                                showStatus={false}
                            >
                                {dataArticle.map(item => (
                                    <div key={item.id}>
                                        <Link to={`/news/${item.id}`}>
                                            <Card
                                                cover={
                                                    <img
                                                        src={`${window.location.origin}/storage/${item.image}`}
                                                        style={{
                                                            width: "85vw"
                                                        }}
                                                    />
                                                }
                                            >
                                                <div className="news-article-mobile">
                                                    <Typography className="header-news-mobile">
                                                        {item.title}
                                                    </Typography>
                                                    <Typography className="news-article-read-mobile">
                                                        {item.desc}
                                                    </Typography>
                                                </div>
                                            </Card>
                                        </Link>
                                    </div>
                                ))}
                            </Carousel>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}
export default Section4Mobile;
