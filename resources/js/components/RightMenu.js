import React, { Component } from "react";
import { Menu } from "antd";
import { Link } from "react-router-dom";

import {
    LinkedinFilled,
    InstagramFilled,
    TwitterCircleFilled
} from "@ant-design/icons";

class RightMenu extends Component {
    render() {
        return (
            <Menu mode="horizontal">
                <Menu.Item key="mail">
                    <Link to="/" className="menu-top">
                        Beranda
                    </Link>
                </Menu.Item>
                <Menu.Item key="protools" className="menu-top">
                    <Link to="/project">Proyek</Link>
                </Menu.Item>
                <Menu.Item key="blog" className="menu-top">
                    <Link to="/about">Tentang Kami</Link>
                </Menu.Item>
                <Menu.Item key="help" className="menu-top">
                    <a
                        href="https://www.linkedin.com/company/properingroup/about/"
                        target="_blank"
                    >
                        <LinkedinFilled style={{ fontSize: "1.4vw" }} />
                    </a>
                </Menu.Item>
                <Menu.Item key="sign">
                    <a
                        href="https://www.instagram.com/properincom/"
                        target="_blank"
                    >
                        <InstagramFilled style={{ fontSize: "1.4vw" }} />
                    </a>
                </Menu.Item>
                <Menu.Item key="login">
                    <a href="https://twitter.com/properingroup" target="_blank">
                        <TwitterCircleFilled style={{ fontSize: "1.4vw" }} />
                    </a>
                </Menu.Item>
            </Menu>
        );
    }
}

export default RightMenu;
