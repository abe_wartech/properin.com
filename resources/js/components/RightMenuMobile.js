import React, { Component } from "react";
import { Menu } from "antd";
import { Link } from "react-router-dom";

import {
    LinkedinFilled,
    InstagramFilled,
    TwitterCircleFilled
} from "@ant-design/icons";

class RightMenuMobile extends Component {
    render() {
        return (
            <div>
                <img
                    src={`${window.location.pathname}images/properin-white.svg`}
                    style={{ paddingTop: "2rem", paddingLeft: "2.5rem" }}
                />
                <Menu
                    style={{ paddingTop: "3rem" }}
                    defaultOpenKeys={["sub1"]}
                    mode="inline"
                >
                    <Menu.Item key="sub1">
                        <Link
                            to="/"
                            style={{ color: "white", fontSize: "4.5vw" }}
                        >
                            Beranda
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="sub2">
                        <Link
                            to="/project"
                            style={{ color: "white", fontSize: "4.5vw" }}
                        >
                            Proyek
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="sub3">
                        <Link
                            to="/about"
                            style={{ color: "white", fontSize: "4.5vw" }}
                        >
                            Tentang Kami
                        </Link>
                    </Menu.Item>
                </Menu>
                <div align="center" className="container-icon">
                    <a
                        href="https://www.linkedin.com/company/properingroup/about/"
                        target="_blank"
                    >
                        <LinkedinFilled className="icon-sosmed" />
                    </a>
                    <a href="" target="https://www.instagram.com/properincom/">
                        <InstagramFilled className="icon-sosmed" />
                    </a>
                    <a href="https://twitter.com/properingroup" target="_blank">
                        <TwitterCircleFilled className="icon-sosmed" />
                    </a>
                </div>
            </div>
        );
    }
}

export default RightMenuMobile;
