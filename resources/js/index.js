import React, { Component } from "react";
import { Switch, HashRouter, Route } from "react-router-dom";
import Home from "./pages/Home";
import Project from "./pages/Project";
import ProjectDetail from "./pages/ProjectDetail";
import About from "./pages/About";
import News from "./pages/News";
import ScrollToTop from "./ScrollToTop";
import "aos/dist/aos.css";
import "antd/dist/antd.less";
import "./App.scss";

class App extends Component {
    render() {
        return (
            <HashRouter>
                <ScrollToTop />
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/project" component={Project} />
                    <Route
                        exact
                        path="/projectdetail"
                        component={ProjectDetail}
                    />
                    <Route exact path="/about" component={About} />
                    <Route exact path="/news/:id" component={News} />
                </Switch>
            </HashRouter>
        );
    }
}

export default App;
