import React, { Component } from "react";
import {
    BackTop,
    Row,
    Col,
    Typography,
    Button,
    Divider,
    Card,
    Progress,
    Menu
} from "antd";
import Navbar from "../components/Navbar";

import NavbarMobile from "../components/NavbarMobile";
import Footer from "../components/Footer";
import FooterMobile from "../components/FooterMobile";
import AOS from "aos";

class About extends Component {
    state = {
        width: window.innerWidth
    };
    componentDidMount = () => {
        AOS.init({
            duration: 1000,
            delay: 50
        });
    };
    render() {
        const { width } = this.state;
        const isMobile = width <= 500;
        //untuk pc//
        if (!isMobile) {
            return (
                <React.Fragment>
                    <Navbar></Navbar>
                    <Row
                        className="aboutsection1"
                        type="flex"
                        justify="center"
                        align="middle"
                    >
                        <Col xs={24} md={24}>
                            <Typography
                                data-aos="zoom-in-up"
                                className="about-top"
                            >
                                Tentang Properin
                            </Typography>
                        </Col>
                    </Row>
                    <Row className="aboutsection2" align="middle">
                        <Col md={12} align="left" data-aos="slide-right">
                            <img
                                src={`${window.location.pathname}images/about.jpeg`}
                                style={{ width: "95%" }}
                            />
                        </Col>
                        <Col md={12} align="left" data-aos="slide-left">
                            <Typography className="about-properin">
                                Properin.com merupakan sebuah online platform
                                untuk berinvestasi di bidang properti yang
                                menghubungkan antara pihak investor dengan pihak
                                yang memerlukan pembiayaan proyek maupun
                                renovasi properti untuk dijual kembali
                                (flipping) melalui proses bagi hasil yang mudah
                                dana aman.
                            </Typography>
                        </Col>
                    </Row>
                    <Row
                        className="aboutsection3"
                        align="middle"
                        justify="space-between"
                    >
                        <Col md={6} data-aos="slide-right">
                            <Typography className="about-eqp">
                                Kelengkapan yang
                                <br />
                                Harus Dianjurkan
                            </Typography>
                        </Col>
                        <Col md={16} data-aos="slide-left">
                            <Row className="about-row" justify="space-between">
                                <Col md={6} className="about-img">
                                    <img
                                        src={`${window.location.pathname}images/id-card.svg`}
                                    />
                                    <Typography className="eqp-info">
                                        Data Legal
                                    </Typography>
                                </Col>
                                <Col md={6} className="about-img">
                                    <img
                                        src={`${window.location.pathname}images/building-icon.svg`}
                                    />
                                    <Typography className="eqp-info">
                                        Dokumentasi Proyek
                                        <br />
                                        Sebelumnya
                                    </Typography>
                                </Col>
                                <Col md={6} className="about-img">
                                    <img
                                        src={`${window.location.pathname}images/rekening-icon.svg`}
                                    />
                                    <Typography className="eqp-info">
                                        Mutasi Rekening
                                    </Typography>
                                </Col>
                                <Col md={6} className="about-img">
                                    <img
                                        src={`${window.location.pathname}images/handshake.svg`}
                                    />
                                    <Typography className="eqp-info">
                                        Bukti Perjanjian Harga
                                        <br />
                                        Beli Properti
                                    </Typography>
                                </Col>
                            </Row>
                            <Row className="about-row">
                                <Col md={6} className="about-img">
                                    <img
                                        src={`${window.location.pathname}images/documentation.svg`}
                                    />
                                    <Typography className="eqp-info">
                                        Dokumentasi Property
                                    </Typography>
                                </Col>
                                <Col md={6} className="about-img">
                                    <img
                                        src={`${window.location.pathname}images/bussiness-plan.svg`}
                                    />
                                    <Typography className="eqp-info">
                                        Business Plan
                                    </Typography>
                                </Col>
                                <Col md={6} className="about-img">
                                    <img
                                        src={`${window.location.pathname}images/renovasi-icon.svg`}
                                    />
                                    <Typography className="eqp-info">
                                        RAB Renovasi
                                        <br />/ Konstruksi
                                    </Typography>
                                </Col>
                                <Col md={6} className="about-img">
                                    <img
                                        src={`${window.location.pathname}images/money.svg`}
                                    />
                                    <Typography className="eqp-info">
                                        Biaya Awal
                                    </Typography>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row
                        className="aboutsection4"
                        align="middle"
                        data-aos="slide-right"
                    >
                        <Col md={14} data-aos="slide-right">
                            <img
                                src={`${window.location.pathname}images/schemainvest.png`}
                                className="img-schema"
                            />
                        </Col>
                        <Col md={10}>
                            <Typography align="right" className="about-schema">
                                Skema
                                <br />
                                Investasi
                            </Typography>
                        </Col>
                    </Row>
                    <Footer />
                </React.Fragment>
            );
        } else {
            //untuk mobile//
            return (
                <div>
                    <NavbarMobile></NavbarMobile>
                    <Row
                        className="aboutsection1-mobile"
                        type="flex"
                        justify="center"
                        align="middle"
                    >
                        <Col xs={24} md={24}>
                            <Typography
                                data-aos="zoom-in-up"
                                className="about-top-mobile"
                            >
                                Tentang Properin
                            </Typography>
                        </Col>
                    </Row>
                    <Row className="aboutsection2-mobile" align="middle">
                        <Col
                            xs={24}
                            md={24}
                            align="center"
                            data-aos="slide-right"
                        >
                            <img
                                src={`${window.location.pathname}images/about.png`}
                                style={{ paddingBottom: "1rem", width: "100%" }}
                            />
                        </Col>
                        <Col xs={24} md={24} align="left" data-aos="slide-left">
                            <Typography>
                                Properin.com merupakan sebuah online platform
                                untuk berinvestasi di bidang properti yang
                                menghubungkan antara pihak investor dengan pihak
                                yang memerlukan pembiayaan proyek maupun
                                renovasi properti untuk dijual kembali
                                (flipping) melalui proses bagi hasil yang mudah
                                dana aman.
                            </Typography>
                        </Col>
                    </Row>
                    <Row
                        className="aboutsection3-mobile"
                        align="middle"
                        justify="space-around"
                    >
                        <Col
                            xs={24}
                            md={24}
                            data-aos="slide-right"
                            align="center"
                        >
                            <Typography className="about-eqp-mobile">
                                Kelengkapan yang Harus Dianjurkan
                            </Typography>
                        </Col>
                        <Col xs={24} md={24} data-aos="slide-left">
                            <Row className="about-row" justify="space-between">
                                <Col
                                    xs={12}
                                    md={12}
                                    className="about-img-mobile"
                                >
                                    <img
                                        src={`${window.location.pathname}images/id-card-mobile.svg`}
                                    />
                                    <Typography className="eqp-info">
                                        Data Legal
                                    </Typography>
                                </Col>
                                <Col
                                    xs={12}
                                    md={12}
                                    className="about-img-mobile"
                                >
                                    <img
                                        src={`${window.location.pathname}images/building-icon-mobile.svg`}
                                    />
                                    <Typography className="eqp-info">
                                        Dokumentasi Proyek
                                        <br />
                                        Sebelumnya
                                    </Typography>
                                </Col>
                                <Col
                                    xs={12}
                                    md={12}
                                    className="about-img-mobile"
                                >
                                    <img
                                        src={`${window.location.pathname}images/rekening-icon-mobile.svg`}
                                    />
                                    <Typography className="eqp-info-mobile">
                                        Mutasi Rekening
                                    </Typography>
                                </Col>
                                <Col
                                    xs={12}
                                    md={12}
                                    className="about-img-mobile"
                                >
                                    <img
                                        src={`${window.location.pathname}images/handshake-mobile.svg`}
                                    />
                                    <Typography className="eqp-info-mobile">
                                        Bukti Perjanjian Harga
                                        <br />
                                        Beli Properti
                                    </Typography>
                                </Col>
                            </Row>
                            <Row className="about-row">
                                <Col
                                    xs={12}
                                    md={12}
                                    className="about-img-mobile"
                                >
                                    <img
                                        src={`${window.location.pathname}images/documentation-mobile.svg`}
                                    />
                                    <Typography className="eqp-info-mobile">
                                        Dokumentasi Property
                                    </Typography>
                                </Col>
                                <Col
                                    xs={12}
                                    md={12}
                                    className="about-img-mobile"
                                >
                                    <img
                                        src={`${window.location.pathname}images/bussiness-plan-mobile.svg`}
                                    />
                                    <Typography className="eqp-info-mobile">
                                        Business Plan
                                    </Typography>
                                </Col>
                                <Col
                                    xs={12}
                                    md={12}
                                    className="about-img-mobile"
                                >
                                    <img
                                        src={`${window.location.pathname}images/renovasi-icon-mobile.svg`}
                                    />
                                    <Typography className="eqp-info-mobile">
                                        RAB Renovasi
                                        <br />/ Konstruksi
                                    </Typography>
                                </Col>
                                <Col
                                    xs={12}
                                    md={12}
                                    className="about-img-mobile"
                                >
                                    <img
                                        src={`${window.location.pathname}images/money-mobile.svg`}
                                    />
                                    <Typography className="eqp-info-mobile">
                                        Biaya Awal
                                    </Typography>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row
                        className="aboutsection4-mobile"
                        align="middle"
                        data-aos="slide-right"
                    >
                        <Col
                            xs={24}
                            md={24}
                            align="center"
                            style={{ paddingBottom: "1rem" }}
                        >
                            <Typography style={{ fontSize: "5vw" }}>
                                Skema Investasi
                            </Typography>
                            <img
                                src={`${window.location.origin}/images/ornament.png`}
                            />
                        </Col>
                        <Col xs={24} md={24} data-aos="slide-right">
                            <img
                                src={`${window.location.pathname}images/schema1-mobile.svg`}
                                className="img-schema-mobile"
                            />
                        </Col>
                        <Col xs={24} md={24} data-aos="slide-right">
                            <img
                                src={`${window.location.pathname}images/schema2-mobile.svg`}
                                className="img-schema-mobile"
                            />
                        </Col>
                        <Col xs={24} md={24} data-aos="slide-right">
                            <img
                                src={`${window.location.pathname}images/schema3-mobile.svg`}
                                className="img-schema-mobile"
                            />
                        </Col>
                        <Col xs={24} md={24} data-aos="slide-right">
                            <img
                                src={`${window.location.pathname}images/schema4-mobile.svg`}
                                className="img-schema-mobile"
                            />
                        </Col>
                        <Col xs={24} md={24} data-aos="slide-right">
                            <img
                                src={`${window.location.pathname}images/schema5-mobile.svg`}
                                className="img-schema-mobile"
                            />
                        </Col>
                    </Row>
                    <FooterMobile />
                    <BackTop visibilityHeight={450} />
                </div>
            );
        }
    }
}

export default About;
