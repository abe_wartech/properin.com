import React, { Component } from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { BackTop } from "antd";
import Footer from "../components/Footer";
import FooterMobile from "../components/FooterMobile";
import Navbar from "../components/Navbar";
import NavbarMobile from "../components/NavbarMobile";
import Section1 from "../components/Beranda/Section1";
import Section1Mobile from "../components/Beranda/Section1Mobile";
import Section2 from "../components/Beranda/Section2";
import Section2Mobile from "../components/Beranda/Section2Mobile";
import Section3 from "../components/Beranda/Section3";
import Section3Mobile from "../components/Beranda/Section3Mobile";
import Section4 from "../components/Beranda/Section4";
import Section4Mobile from "../components/Beranda/Section4Mobile";
import { inject, observer } from "mobx-react";
import AOS from "aos";

@inject("newsStore")
@observer
class Home extends Component {
    state = {
        width: window.innerWidth,
        visible: false,
        confirmLoading: false
    };
    componentDidMount = () => {
        AOS.init({
            duration: 1000,
            delay: 50
        });
        this.props.newsStore.news();
        this.props.newsStore.article();
        this.props.newsStore.home();
    };

    showModal = () => {
        this.setState({
            visible: true
        });
    };
    handleOk = () => {
        this.setState({
            confirmLoading: true
        });
        setTimeout(() => {
            this.setState({
                visible: false,
                confirmLoading: false
            });
        }, 2000);
    };
    handleCancel = () => {
        this.setState({
            visible: false
        });
    };

    UNSAFE_componentWillMount() {
        window.addEventListener("resize", this.handleWindowSizeChange);
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.handleWindowSizeChange);
    }

    handleWindowSizeChange = () => {
        this.setState({ width: window.innerWidth });
    };
    render() {
        const { width } = this.state;
        const isMobile = width <= 500;
        //untuk pc//
        if (!isMobile) {
            return (
                <div>
                    <Navbar></Navbar>
                    <Section1 />
                    <Section2 />
                    <Section3 />
                    <Section4 />
                    <Footer />
                    <BackTop visibilityHeight={450} />
                </div>
            );
        } else {
            //untuk mobile//
            return (
                <div>
                    <NavbarMobile></NavbarMobile>
                    <Section1Mobile />
                    <Section2Mobile />
                    <Section3Mobile />
                    <Section4Mobile />
                    <FooterMobile />
                    <BackTop visibilityHeight={450} />
                </div>
            );
        }
    }
}

export default Home;
