import React, { Component } from "react";
import { BackTop, Row, Col, Typography, Button, Card } from "antd";
import { Link } from "react-router-dom";
import Slider from "react-slick";
import Footer from "../components/Footer";
import FooterMobile from "../components/FooterMobile";
import Navbar from "../components/Navbar";
import NavbarMobile from "../components/NavbarMobile";
import { inject, observer } from "mobx-react";
import AOS from "aos";

const { Title } = Typography;

@inject("newsStore")
@observer
class News extends Component {
    state = {
        width: window.innerWidth
    };

    componentDidMount = () => {
        const idUrl = window.location.hash;
        const data = {
            id: idUrl.replace("#/news/", "")
        };
        AOS.init({
            duration: 1000,
            delay: 50
        });
        this.props.newsStore.onearticle(data);
        this.props.newsStore.article();
    };

    componentDidUpdate = () => {
        const idUrl = window.location.hash;
        const data = {
            id: idUrl.replace("#/news/", "")
        };
        this.props.newsStore.onearticle(data);
    };

    UNSAFE_componentWillMount() {
        window.addEventListener("resize", this.handleWindowSizeChange);
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.handleWindowSizeChange);
    }

    handleWindowSizeChange = () => {
        this.setState({ width: window.innerWidth });
    };
    render() {
        const settings = {
            dots: false,
            infinite: true,
            centerMode: true,
            slidesToShow: 1,
            variableWidth: true,
            speed: 500,
            autoPlay: true
        };
        const { width } = this.state;
        const isMobile = width <= 500;
        const { dataOneArticle, dataArticle } = this.props.newsStore;
        //untuk pc//
        if (!isMobile) {
            return (
                <div>
                    <Navbar></Navbar>
                    <Row justify="center" className="row-news">
                        <Col
                            md={24}
                            data-aos="slide-right"
                            style={{ marginTop: 50 }}
                        >
                            <Typography className="news-info">
                                {dataOneArticle.title}
                            </Typography>
                            <Typography className="date-news">
                                {dataOneArticle.created_at}
                            </Typography>
                            <img
                                src={`${window.location.origin}/storage/${dataOneArticle.image}`}
                                className="img-news"
                            />
                            <div
                                dangerouslySetInnerHTML={{
                                    __html: dataOneArticle.content
                                }}
                            />
                        </Col>
                    </Row>
                    <Row className="sectionnews" justify="center">
                        <Row
                            type="flex"
                            gutter={48}
                            justify="space-around"
                            data-aos="slide-right"
                        >
                            <Col md={24}>
                                <Title className="another-news">
                                    Berita & Artikel Lainnya
                                </Title>
                            </Col>
                            {dataArticle.map(item => (
                                <Col md={6} align="center">
                                    <Link to={`/news/${item.id}`}>
                                        <Card
                                            className="news-place"
                                            cover={
                                                <img
                                                    src={`${window.location.origin}/storage/${item.image}`}
                                                />
                                            }
                                        >
                                            <div className="news-article">
                                                <Typography className="header-news">
                                                    {item.title}
                                                </Typography>
                                                <Typography className="news-article-read">
                                                    {item.desc}
                                                </Typography>
                                            </div>
                                        </Card>
                                    </Link>
                                </Col>
                            ))}
                        </Row>
                    </Row>
                    <Row data-aos="fade-down-right">
                        <Col md={24} align="center" className="link-to-home">
                            <Link to="/">
                                <Button className="button-news">
                                    Kembali Ke Beranda
                                </Button>
                            </Link>
                        </Col>
                    </Row>
                    <Footer />
                    <BackTop visibilityHeight={450} />
                </div>
            );
        } else {
            //untuk mobile//
            return (
                <div>
                    <NavbarMobile></NavbarMobile>
                    <Row justify="center" className="row-news-mobile">
                        <Col xs={24} md={24} data-aos="slide-right">
                            <Typography className="news-info-mobile">
                                {dataOneArticle.title}
                            </Typography>
                            <Typography className="date-news-mobile">
                                {dataOneArticle.created_at}
                            </Typography>
                            <img
                                src={`${window.location.origin}/storage/${dataOneArticle.image}`}
                                className="img-news"
                            />
                            <Typography className="news-mobile">
                                <div
                                    dangerouslySetInnerHTML={{
                                        __html: dataOneArticle.content
                                    }}
                                />
                            </Typography>
                        </Col>
                    </Row>
                    <Row className="sectionnews-mobile" justify="center">
                        <Row
                            type="flex"
                            gutter={48}
                            justify="space-around"
                            data-aos="slide-right"
                        >
                            <Col xs={24} md={24}>
                                <Typography className="another-news-mobile">
                                    Berita & Artikel Lainnya
                                </Typography>
                            </Col>
                            <img
                                src={`${window.location.origin}/images/ornament.png`}
                            />
                        </Row>
                    </Row>
                    <Row>
                        <Col xs={24} md={24} align="center">
                            <div style={{ height: "44vh" }}>
                                <Slider {...settings}>
                                    {dataArticle.map(item => (
                                        <div>
                                            <Link to={`/news/${item.id}`}>
                                                <Card
                                                    style={{
                                                        marginLeft: "2rem",
                                                        marginRight: "2rem"
                                                    }}
                                                    cover={
                                                        <img
                                                            src={`${window.location.origin}/storage/${item.image}`}
                                                            style={{
                                                                width: "85vw"
                                                            }}
                                                        />
                                                    }
                                                >
                                                    <div className="news-article-mobile">
                                                        <Typography className="header-news-mobile-another">
                                                            {item.title}
                                                        </Typography>
                                                    </div>
                                                </Card>
                                            </Link>
                                        </div>
                                    ))}
                                </Slider>
                            </div>
                        </Col>
                    </Row>
                    <Row data-aos="fade-down-right">
                        <Col
                            xs={24}
                            md={24}
                            align="center"
                            className="link-to-home-mobile"
                        >
                            <Link to="/">
                                <Button className="button-news-mobile" block>
                                    Kembali Ke Beranda
                                </Button>
                            </Link>
                        </Col>
                    </Row>
                    <FooterMobile />
                    <BackTop visibilityHeight={450} />
                </div>
            );
        }
    }
}

export default News;
