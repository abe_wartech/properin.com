import React, { Component } from "react";
import {
    BackTop,
    Row,
    Col,
    Typography,
    Card,
    Progress,
    Divider,
    Button,
    Modal,
    Form,
    Input,
    message,
} from "antd";
import Navbar from "../components/Navbar";
import NavbarMobile from "../components/NavbarMobile";
import Footer from "../components/Footer";
import FooterMobile from "../components/FooterMobile";
import AOS from "aos";
const { Title } = Typography;
import { Link } from "react-router-dom";
import { inject, observer } from "mobx-react";

@inject("newsStore")
@observer
class Project extends Component {
    state = {
        width: window.innerWidth,
        visible: false,
        confirmLoading: false,
        currentPage: 1,
        todosPerPage: 3,
        visible2: false,
        project_id: "",
        project_name: "",
        no_tlp: "",
        email: "",
        confirmLoading2: false,
    };
    componentDidMount = () => {
        AOS.init({
            duration: 1000,
            delay: 50,
        });
        this.props.newsStore.allproject();
    };

    showModal = (id, name) => {
        this.setState({
            visible: true,
            project_id: id,
            project_name: name,
        });
    };
    showModal2 = () => {
        if (this.state.no_tlp === "") {
            message.error("Please enter your no tlp!");
        }
        if (this.state.email === "") {
            message.error("Please enter your email!");
        } else {
            this.setState({
                visible2: true,
                visible: false,
            });
            const data = {
                project_id: this.state.project_id,
                project_name: this.state.project_name,
                no_tlp: this.state.no_tlp,
                email: this.state.email,
            };
            this.props.newsStore.invest(data);
        }
    };
    handleOk = () => {
        this.setState({
            confirmLoading: true,
        });
        setTimeout(() => {
            this.setState({
                visible: false,
                confirmLoading: false,
            });
        }, 2000);
    };
    handleOk2 = () => {
        this.setState({
            confirmLoading2: true,
        });
        setTimeout(() => {
            this.setState({
                visible2: false,
                confirmLoading2: false,
            });
        }, 2000);
    };
    handleCancel = () => {
        this.setState({
            visible: false,
        });
    };
    handleCancel2 = () => {
        this.setState({
            visible2: false,
        });
    };
    handleClick = (event) => {
        this.setState({
            currentPage: Number(event.target.id),
        });
    };
    onChangeTelp = (event) => {
        this.setState({ no_tlp: event.target.value });
    };
    onChangeEmail = (event) => {
        this.setState({ email: event.target.value });
    };
    render() {
        const {
            visible,
            confirmLoading,
            width,
            currentPage,
            todosPerPage,
            visible2,
            confirmLoading2,
        } = this.state;
        const isMobile = width <= 500;
        const { dataAllProject } = this.props.newsStore;
        const indexOfLastTodo = currentPage * todosPerPage;
        const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
        const currentTodos = dataAllProject.slice(
            indexOfFirstTodo,
            indexOfLastTodo
        );
        const pageNumbers = [];
        for (
            let i = 1;
            i <= Math.ceil(dataAllProject.length / todosPerPage);
            i++
        ) {
            pageNumbers.push(i);
        }
        const renderPageNumbers = pageNumbers.map((number) => {
            return (
                <li key={number} id={number} onClick={this.handleClick}>
                    {number}
                </li>
            );
        });
        const renderPageNumbersMobile = pageNumbers.map((number) => {
            return (
                <li key={number} id={number} onClick={this.handleClick}>
                    {number}
                </li>
            );
        });
        //untuk pc//
        if (!isMobile) {
            return (
                <React.Fragment>
                    <Navbar></Navbar>
                    <Row
                        className="projectsection1"
                        type="flex"
                        justify="center"
                        align="middle"
                    >
                        <Col xs={24} md={24}>
                            <Typography
                                data-aos="zoom-in-up"
                                className="about-top"
                            >
                                Proyek Pembangunan Tersedia
                                <br />
                                Untuk di Invest
                            </Typography>
                        </Col>
                    </Row>
                    <Row
                        type="flex"
                        gutter={48}
                        justify="space-around"
                        data-aos="slide-right"
                        className="section-project"
                    >
                        {currentTodos.map((item) => (
                            <Col md={8} align="center" key={item.id}>
                                <Card
                                    className="card-invest"
                                    cover={
                                        <img
                                            src={`${window.location.origin}/storage/${item.image}`}
                                        />
                                    }
                                >
                                    <Typography className="title-invest">
                                        {item.name}
                                    </Typography>
                                    <div className="padding-invest">
                                        <img
                                            src={`${window.location.origin}/images/label.svg`}
                                            className="textinfo"
                                        />
                                        <Typography className="textinfodesc">
                                            {item.harga}
                                        </Typography>
                                        <img
                                            src={`${window.location.origin}/images/loc.svg`}
                                            className="textinfo"
                                        />
                                        <Typography className="textinfodesc">
                                            {item.tempat}
                                        </Typography>
                                    </div>
                                    <div className="padding-invest">
                                        <Typography className="textinfoprogress">
                                            Pledge
                                        </Typography>
                                        <Progress percent={item.pledge} />
                                        <Typography>Raised</Typography>
                                        <Progress
                                            percent={item.raised}
                                            style={{ marginBottom: "2rem" }}
                                        />
                                        <br />
                                        <Typography className="insideleft">
                                            Projected Return
                                        </Typography>
                                        <Typography className="inside">
                                            {item.return}
                                        </Typography>
                                        <Divider />
                                        <Typography className="insideleft">
                                            Duration
                                        </Typography>
                                        <Typography className="inside">
                                            {item.duration}
                                        </Typography>
                                        <Divider />
                                        <Typography className="insideleft">
                                            Goal
                                        </Typography>
                                        <Typography className="inside">
                                            {item.goal}
                                        </Typography>
                                        <Divider />
                                        <Typography className="insideleft">
                                            Raised
                                        </Typography>
                                        <Typography className="inside">
                                            {item.raised_idr}
                                        </Typography>
                                        <Divider />
                                        <Typography className="insideleft">
                                            Time Remaining
                                        </Typography>
                                        <Typography className="inside">
                                            {item.time_remaining}
                                        </Typography>
                                        <Divider />
                                    </div>
                                    <Button
                                        className="hoverbutton"
                                        block
                                        onClick={() => {
                                            this.showModal(item.id, item.name);
                                        }}
                                    >
                                        Invest
                                    </Button>
                                </Card>
                            </Col>
                        ))}
                    </Row>
                    <ul id="page-numbers">{renderPageNumbers}</ul>
                    <Modal
                        width={"75vw"}
                        visible={visible}
                        onOk={this.handleOk}
                        confirmLoading={confirmLoading}
                        onCancel={this.handleCancel}
                        footer={null}
                    >
                        <Row style={{ padding: "1rem 3rem" }} align="middle">
                            <Col md={12}>
                                <img
                                    src={`${window.location.origin}/images/popupinvest.svg`}
                                />
                                <Title
                                    style={{
                                        fontSize: "1.4vw",
                                        paddingTop: "1rem",
                                    }}
                                >
                                    Masukkan Informasi Kontak Untuk
                                    <br />
                                    Memulai Investasi Anda di{" "}
                                    <span style={{ color: "#23ad46" }}>
                                        Properin
                                    </span>
                                </Title>
                            </Col>
                            <Col md={12} style={{ paddingLeft: "4rem" }}>
                                <Form>
                                    <Form.Item>
                                        <Title style={{ fontSize: "1vw" }}>
                                            Nomer Telepon
                                        </Title>
                                        <Input
                                            onChange={this.onChangeTelp}
                                            placeholder="0812 3846 xxxx"
                                        />
                                    </Form.Item>
                                    <Form.Item>
                                        <Title style={{ fontSize: "1vw" }}>
                                            E-Mail
                                        </Title>
                                        <Input
                                            onChange={this.onChangeEmail}
                                            placeholder="contoh@gmail.com"
                                        />
                                    </Form.Item>
                                </Form>
                                <Row justify="end">
                                    <Col>
                                        <Button
                                            style={{ color: "#23ad46" }}
                                            type="link"
                                            onClick={this.handleCancel}
                                        >
                                            Batal
                                        </Button>
                                        <Button
                                            onClick={this.showModal2}
                                            type="primary"
                                            style={{
                                                backgroundColor: "#23ad46",
                                                border: "none",
                                            }}
                                        >
                                            Kirim
                                        </Button>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Modal>
                    <Modal
                        width={"75vw"}
                        visible={visible2}
                        onOk={this.handleOk2}
                        confirmLoading={confirmLoading2}
                        onCancel={this.handleCancel2}
                        footer={null}
                    >
                        <Row
                            className="modal-popup-thank"
                            align="middle"
                            justify="center"
                        >
                            <Col md={24}>
                                <img
                                    src={`${window.location.origin}/images/checklist.svg`}
                                    style={{ width: "20vw", height: "25vh" }}
                                />
                                <Typography
                                    style={{
                                        fontSize: "2vw",
                                        paddingTop: "1rem",
                                        fontWeight: "bold",
                                    }}
                                >
                                    Terimakasih
                                </Typography>
                                <Typography
                                    style={{
                                        fontSize: "1.6vw",
                                        paddingBottom: "1rem",
                                    }}
                                >
                                    Team kami akan segera menghubungi anda
                                </Typography>

                                <Button
                                    className="button-more-thank"
                                    onClick={this.handleCancel2}
                                >
                                    Kembali
                                </Button>
                            </Col>
                        </Row>
                    </Modal>
                    <Footer></Footer>
                </React.Fragment>
            );
        } else {
            //untuk mobile//
            return (
                <div>
                    <NavbarMobile></NavbarMobile>
                    <Row
                        className="projectsection1-mobile"
                        type="flex"
                        justify="center"
                        align="middle"
                    >
                        <Col xs={24} md={24}>
                            <Typography
                                data-aos="zoom-in-up"
                                className="about-top-mobile"
                            >
                                Proyek Pembangunan Tersedia
                                <br />
                                Untuk di Invest
                            </Typography>
                        </Col>
                    </Row>
                    <Row className="project-mobile" justify="center">
                        {currentTodos.map((item) => (
                            <Row
                                type="flex"
                                gutter={48}
                                style={{ paddingBottom: "1rem" }}
                            >
                                <Col md={8} align="center">
                                    <Card
                                        cover={
                                            <img
                                                src={`${window.location.origin}/storage/${item.image}`}
                                            />
                                        }
                                    >
                                        <Typography className="title-invest-mobile">
                                            {item.name}
                                        </Typography>
                                        <div className="padding-invest-mobile">
                                            <img
                                                src={`${window.location.origin}/images/label.svg`}
                                                className="textinfo-mobile"
                                            />
                                            <Typography className="textinfodesc-mobile">
                                                {item.harga}
                                            </Typography>
                                            <img
                                                src={`${window.location.origin}/images/loc.svg`}
                                                className="textinfo-mobile"
                                            />
                                            <Typography className="textinfodesc-mobile">
                                                {item.tempat}
                                            </Typography>
                                        </div>
                                        <div className="padding-invest-mobile">
                                            <Typography>Raised</Typography>
                                            <Progress
                                                percent={item.raised}
                                                style={{
                                                    marginBottom: "1rem",
                                                }}
                                            />
                                            <br />
                                        </div>
                                        <Link to="projectdetail">
                                            <Button
                                                className="button-mobile"
                                                type="link"
                                                block
                                            >
                                                Detail
                                            </Button>
                                        </Link>
                                        <Button
                                            className="hoverbutton-mobile"
                                            block
                                            onClick={() => {
                                                this.showModal(
                                                    item.id,
                                                    item.name
                                                );
                                            }}
                                        >
                                            Invest
                                        </Button>
                                    </Card>
                                </Col>
                            </Row>
                        ))}
                        <ul id="page-numbers-mobile">
                            {renderPageNumbersMobile}
                        </ul>
                    </Row>
                    <Modal
                        width={"80vw"}
                        visible={visible}
                        onOk={this.handleOk}
                        confirmLoading={confirmLoading}
                        onCancel={this.handleCancel}
                        footer={null}
                    >
                        <Row align="middle">
                            <Col xs={24} md={24} align="center">
                                <img
                                    src={`${window.location.origin}/images/popupinvest.svg`}
                                    style={{ width: "57vw", height: "50vw" }}
                                />
                            </Col>
                            <Col xs={24}>
                                <Typography
                                    style={{
                                        fontSize: "4vw",
                                        textAlign: "center",
                                        paddingBottom: "1rem",
                                    }}
                                >
                                    Masukkan Informasi Kontak Untuk Memulai
                                    Investasi Anda di{" "}
                                    <span style={{ color: "#23ad46" }}>
                                        Properin
                                    </span>
                                </Typography>
                            </Col>
                            <Col xs={24}>
                                <Form>
                                    <Form.Item>
                                        <Typography style={{ fontSize: "3vw" }}>
                                            Nomer Telepon
                                        </Typography>
                                        <Input
                                            onChange={this.onChangeTelp}
                                            placeholder="0812 3846 xxxx"
                                        />
                                    </Form.Item>
                                    <Form.Item>
                                        <Typography style={{ fontSize: "3vw" }}>
                                            E-Mail
                                        </Typography>
                                        <Input
                                            onChange={this.onChangeEmail}
                                            placeholder="contoh@gmail.com"
                                        />
                                    </Form.Item>
                                </Form>
                                <Row justify="space-between">
                                    <Col xs={12}>
                                        <Button
                                            style={{ color: "#23ad46" }}
                                            type="link"
                                            onClick={this.handleCancel}
                                        >
                                            Batal
                                        </Button>
                                    </Col>
                                    <Col xs={12} align="right">
                                        <Button
                                            onClick={this.showModal2}
                                            type="primary"
                                            style={{
                                                backgroundColor: "#23ad46",
                                                width: "20vw",
                                                border: "none",
                                            }}
                                        >
                                            Kirim
                                        </Button>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Modal>
                    <Modal
                        width={"80vw"}
                        visible={visible2}
                        onOk={this.handleOk2}
                        confirmLoading={confirmLoading2}
                        onCancel={this.handleCancel2}
                        footer={null}
                    >
                        <Row align="middle">
                            <Col xs={24} md={24} align="center">
                                <img
                                    src={`${window.location.origin}/images/checklist.svg`}
                                    style={{ width: "40vw", height: "30vh" }}
                                />
                            </Col>
                            <Col xs={24} align="center">
                                <Typography
                                    style={{
                                        fontSize: "5vw",
                                        textAlign: "center",
                                        paddingTop: "1rem",
                                        fontWeight: "bold",
                                    }}
                                >
                                    Terimakasih
                                </Typography>
                                <Typography
                                    style={{
                                        fontSize: "3.5vw",
                                        paddingBottom: "1rem",
                                    }}
                                >
                                    Team kami akan segera menghubungi anda
                                </Typography>
                                <Link to="/project">
                                    <Button
                                        block
                                        onClick={this.handleCancel2}
                                        className="button-more-mobile-thank"
                                    >
                                        Kembali
                                    </Button>
                                </Link>
                            </Col>
                        </Row>
                    </Modal>
                    <FooterMobile />
                    <BackTop visibilityHeight={450} />
                </div>
            );
        }
    }
}

export default Project;
