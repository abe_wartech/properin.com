import React, { Component } from "react";
import {
    BackTop,
    Row,
    Col,
    Typography,
    Card,
    Progress,
    Button,
    Modal,
    Form,
    Input,
    Divider,
} from "antd";
import NavbarMobile from "../components/NavbarMobile";
import FooterMobile from "../components/FooterMobile";
import AOS from "aos";
const { Title } = Typography;
import { Link } from "react-router-dom";
import { inject, observer } from "mobx-react";

@inject("newsStore")
@observer
class ProjectDetail extends Component {
    state = {
        width: window.innerWidth,
        visible: false,
        confirmLoading: false,
        currentPage: 1,
        todosPerPage: 3,
        visible2: false,
        project_id: "",
        project_name: "",
        no_tlp: "",
        email: "",
        confirmLoading2: false,
    };
    componentDidMount = () => {
        AOS.init({
            duration: 1000,
            delay: 50,
        });
    };

    showModal = () => {
        this.setState({
            visible: true,
        });
    };
    showModal2 = () => {
        this.setState({
            visible2: true,
            visible: false,
        });
        const data = {
            project_id: this.state.project_id,
            project_name: this.state.project_name,
            no_tlp: this.state.no_tlp,
            email: this.state.email,
        };
        this.props.newsStore.invest(data);
    };
    handleOk = () => {
        this.setState({
            confirmLoading: true,
        });
        setTimeout(() => {
            this.setState({
                visible: false,
                confirmLoading: false,
            });
        }, 2000);
    };
    handleOk2 = () => {
        this.setState({
            confirmLoading2: true,
        });
        setTimeout(() => {
            this.setState({
                visible2: false,
                confirmLoading2: false,
            });
        }, 2000);
    };
    handleCancel = () => {
        this.setState({
            visible: false,
        });
    };
    handleCancel2 = () => {
        this.setState({
            visible2: false,
        });
    };
    render() {
        const {
            visible,
            confirmLoading,
            width,
            currentPage,
            todosPerPage,
            visible2,
            confirmLoading2,
        } = this.state;
        const isMobile = width <= 500;
        const { dataAllProject } = this.props.newsStore;
        const indexOfLastTodo = currentPage * todosPerPage;
        const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
        const currentTodos = dataAllProject.slice(
            indexOfFirstTodo,
            indexOfLastTodo
        );
        if (isMobile) {
            return (
                <div>
                    <NavbarMobile />
                    <Row
                        type="flex"
                        gutter={48}
                        justify="space-around"
                        data-aos="slide-right"
                        // className="project-detail"
                    >
                        <Col xs={24} align="center">
                            <Card
                                className="card-invest-mobile"
                                cover={
                                    <img
                                        src={`${window.location.origin}/images/bangunan1.png`}
                                    />
                                }
                            >
                                <Typography className="title-invest-mobile">
                                    Hasanah City
                                </Typography>
                                <div className="padding-invest-mobile">
                                    <img
                                        src={`${window.location.origin}/images/label.svg`}
                                        className="textinfo-mobile"
                                    />
                                    <Typography className="textinfodesc-mobile">
                                        Harga Terjangkau
                                    </Typography>
                                    <img
                                        src={`${window.location.origin}/images/loc.svg`}
                                        className="textinfo-mobile"
                                    />
                                    <Typography className="textinfodesc-mobile">
                                        Parung Panjang, Bogor
                                    </Typography>
                                </div>
                                <div className="padding-invest-mobile">
                                    <Typography className="textinfoprogress">
                                        Pledge
                                    </Typography>
                                    <Progress
                                        percent={15}
                                        style={{
                                            marginBottom: "1rem",
                                        }}
                                    />
                                    <Typography>Raised</Typography>
                                    <Progress
                                        percent={15}
                                        style={{
                                            marginBottom: "1rem",
                                        }}
                                    />
                                    <br />
                                    <Typography className="insideleft">
                                        Projected Return
                                    </Typography>
                                    <Typography className="inside">
                                        data
                                    </Typography>
                                    <Divider />
                                    <Typography className="insideleft">
                                        Duration
                                    </Typography>
                                    <Typography className="inside">
                                        data
                                    </Typography>
                                    <Divider />
                                    <Typography className="insideleft">
                                        Goal
                                    </Typography>
                                    <Typography className="inside">
                                        data
                                    </Typography>
                                    <Divider />
                                    <Typography className="insideleft">
                                        Raised
                                    </Typography>
                                    <Typography className="inside">
                                        data
                                    </Typography>
                                    <Divider />
                                    <Typography className="insideleft">
                                        Time Remaining
                                    </Typography>
                                    <Typography className="inside">
                                        data
                                    </Typography>
                                </div>

                                <Button
                                    block
                                    onClick={this.showModal}
                                    className="hover-mobile-detail"
                                >
                                    Invest
                                </Button>
                                <Link to="project">
                                    <Button
                                        type="link"
                                        block
                                        className="hover-mobile-back"
                                    >
                                        Lihat Lebih Banyak
                                    </Button>
                                </Link>
                            </Card>
                        </Col>
                    </Row>
                    <Modal
                        width={"80vw"}
                        visible={visible}
                        onOk={this.handleOk}
                        confirmLoading={confirmLoading}
                        onCancel={this.handleCancel}
                        footer={null}
                    >
                        <Row align="middle">
                            <Col xs={24} md={24} align="center">
                                <img
                                    src={`${window.location.origin}/images/popupinvest.svg`}
                                    style={{ width: "57vw", height: "50vw" }}
                                />
                            </Col>
                            <Col xs={24}>
                                <Typography
                                    style={{
                                        fontSize: "4vw",
                                        textAlign: "center",
                                        paddingBottom: "1rem",
                                    }}
                                >
                                    Masukkan Informasi Kontak Untuk Memulai
                                    Investasi Anda di{" "}
                                    <span style={{ color: "#23ad46" }}>
                                        Properin
                                    </span>
                                </Typography>
                            </Col>
                            <Col xs={24}>
                                <Form>
                                    <Form.Item>
                                        <Typography style={{ fontSize: "3vw" }}>
                                            Nomer Telepon
                                        </Typography>
                                        <Input placeholder="0812 3846 xxxx" />
                                    </Form.Item>
                                    <Form.Item>
                                        <Typography style={{ fontSize: "3vw" }}>
                                            E-Mail
                                        </Typography>
                                        <Input placeholder="contoh@gmail.com" />
                                    </Form.Item>
                                </Form>
                                <Row justify="space-between">
                                    <Col xs={12}>
                                        <Button
                                            style={{ color: "#23ad46" }}
                                            type="link"
                                            onClick={this.handleCancel}
                                        >
                                            Batal
                                        </Button>
                                    </Col>
                                    <Col xs={12} align="right">
                                        <Button
                                            onClick={this.showModal2}
                                            type="primary"
                                            style={{
                                                backgroundColor: "#23ad46",
                                                width: "20vw",
                                                border: "none",
                                            }}
                                        >
                                            Kirim
                                        </Button>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Modal>
                    <Modal
                        width={"80vw"}
                        visible={visible2}
                        onOk={this.handleOk2}
                        confirmLoading={confirmLoading2}
                        onCancel={this.handleCancel2}
                        footer={null}
                    >
                        <Row align="middle">
                            <Col xs={24} md={24} align="center">
                                <img
                                    src={`${window.location.origin}/images/checklist.svg`}
                                    style={{ width: "40vw", height: "30vh" }}
                                />
                            </Col>
                            <Col xs={24} align="center">
                                <Typography
                                    style={{
                                        fontSize: "5vw",
                                        textAlign: "center",
                                        paddingTop: "1rem",
                                        fontWeight: "bold",
                                    }}
                                >
                                    Terimakasih
                                </Typography>
                                <Typography
                                    style={{
                                        fontSize: "3.5vw",
                                        paddingBottom: "1rem",
                                    }}
                                >
                                    Team kami akan segera menghubungi anda
                                </Typography>
                                <Link to="/project">
                                    <Button
                                        block
                                        onClick={this.handleCancel2}
                                        className="button-more-mobile-thank"
                                    >
                                        Kembali
                                    </Button>
                                </Link>
                            </Col>
                        </Row>
                    </Modal>
                    <FooterMobile />
                    <BackTop visibilityHeight={450} />
                </div>
            );
        }
    }
}
export default ProjectDetail;
