import React, { Component } from "react";
import { observable, action, runInAction } from "mobx";

class NewsStore extends Component {
    @observable dataNews = [];
    @observable dataAllProject = [];
    @observable dataArticle = [];
    @observable dataHome = [];
    @observable dataOneArticle = [];

    @action
    news() {
        fetch("api/project", {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    runInAction(() => {
                        this.dataNews = res.message.sort((a, b) =>
                            b.created_at > a.created_at ? 1 : -1
                        );
                    });
                } else {
                    console.log("error");
                }
            })
            .catch(err => console.log(err));
    }

    @action
    allproject() {
        fetch("api/allproject", {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    runInAction(() => {
                        this.dataAllProject = res.message.sort((a, b) =>
                            b.created_at > a.created_at ? 1 : -1
                        );
                    });
                } else {
                    console.log("error");
                }
            })
            .catch(err => console.log(err));
    }

    @action
    article() {
        fetch("api/article", {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    runInAction(() => {
                        this.dataArticle = res.message.sort((a, b) =>
                            b.created_at > a.created_at ? 1 : -1
                        );
                    });
                } else {
                    console.log("error");
                }
            })
            .catch(err => console.log(err));
    }

    @action
    onearticle(data) {
        fetch("api/onearticle", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    runInAction(() => {
                        this.dataOneArticle = res.message[0];
                    });
                } else {
                    console.log("error");
                }
            })
            .catch(err => console.log(err));
    }

    @action
    home() {
        fetch("api/home", {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    runInAction(() => {
                        this.dataHome = res.message;
                    });
                } else {
                    console.log("error");
                }
            })
            .catch(err => console.log(err));
    }

    @action
    invest(data) {
        console.log(data);
        fetch("api/invest", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    runInAction(() => {
                        console.log("sucess");
                    });
                } else {
                    console.log("error");
                }
            })
            .catch(err => console.log(err));
    }
}

export default new NewsStore();
