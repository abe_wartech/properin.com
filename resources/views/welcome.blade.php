<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no" />
    <meta name="theme-color" content="#000000">
    <base href="/" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Properin - Platform Investasi Properti</title>
    <meta name="description" content="Properin adalah platform yang memungkinkan pengguna untuk mendapatkan informasi dan peluang investasi di proyek properti">
    <meta name="google-site-verification" content="VTvtPrKjYk4P9XQsJ6i0KbVbud4OSlrmx-dl09AkfEk" />
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link rel="icon" href="{{asset('favicon.ico')}}" />
    <link rel="stylesheet" type="text/css" charset="UTF-8" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500"> -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
    <link href="{{asset('font/Product-Sans-Regular.ttf')}}" rel="stylesheet">

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ])!!};
    </script>
</head>

<body>
    <noscript>
      You need to enable JavaScript to run this app.
    </noscript>
    <div id="app">
        @yield('content')
    </div>

    <script src="{{mix('/js/app.js')}}"></script>
    <script src="{{mix('/js/manifest.js')}}"></script>
    <script src="{{mix('/js/vendor.js')}}"></script>
</body>

</html>
