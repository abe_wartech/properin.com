<?php

Route::get('project', 'ApiController@project');
Route::get('allproject', 'ApiController@allproject');
Route::get('article', 'ApiController@article');
Route::get('home', 'ApiController@home');
Route::post('onearticle', 'ApiController@oneArticle');
Route::post('invest', 'ApiController@invest');
