<?php

Route::get('/', function () {
    return view('welcome');
});
Route::get('/gen', 'SitemapController@index');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
